import { createContext, useContext, useState } from "react";
import { Storage } from "../utils/storage";

const UserContext = createContext()

export function useUser() {
    return useContext(UserContext)
}

const UserProvider = ({ children }) => {

    const [user, setUser] = useState(Storage.get('username') || '')

    return (
        <UserContext.Provider value={[user, setUser]}>
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider