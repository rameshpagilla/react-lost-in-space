import { Link } from 'react-router-dom'
import { useUser } from '../../context/UserContext'
import styles from './Header.module.css'

const Header = () => {

    const [user] = useUser()

    return (
        <header className={styles.Header}>
            <div>
                <Link to="/translate">
                    <h2 className="title">Lost in Translation</h2>
                </Link>
            </div>
            {user &&
                <div>
                    <Link to="/profile">{user}</Link>
                </div>
            }
        </header>
    )
}
export default Header
