import { useEffect, useState } from "react"
import { useHistory } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import AppContainer from "../../hoc/AppContainer"
import { Storage } from "../../utils/storage"

import ProfileHeader from "./ProfileHeader"

const Profile = () => {

    const [user, setUser] = useUser()
    const [translations, setTranslations ] = useState(Storage.get('translations') || [])
    const history = useHistory()

    const onLogoutClick = () => {
        const { confirm } = window
        if (confirm('Are you sure? Your translations will be lost. 😱')) {
            Storage.delete('username')
            Storage.delete('translations')
            setUser('')
        }
    }

    const onClearClick = () => {
        const { confirm } = window
        if (confirm('Are you sure? Your translations will be lost. 😱')) {
            Storage.delete('translations')
            setTranslations([])
        }
    }

    useEffect(() => {
        if (user === '') {
            history.push('/')
        }
    }, [user, history])

    return (
        <main>
            <ProfileHeader user={ user } logout={ onLogoutClick } clear={ onClearClick } />
            <br />
            <AppContainer>
                <section className="translation-history">
                    <h4>Here are your last 10 translations:</h4>
                    {translations.length === 0 &&
                        <p>No translations yet 😢</p>
                    }
                    <ol>
                        {translations.map((translation, idx) => <li key={idx}>{translation}</li>)}
                    </ol>
                </section>
            </AppContainer>

        </main>
    )
}
export default Profile