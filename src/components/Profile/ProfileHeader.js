import { Link } from 'react-router-dom'
import AppContainer from '../../hoc/AppContainer'
import LogoHello from './Logo-Hello.png'
import styles from './ProfileHeader.module.css'

const ProfileHeader = ({ user, logout, clear }) => {
    return (
        <header className={styles.ProfileHeader}>
            <AppContainer>
                <img src={LogoHello} alt="Robot saying hello" width="100" />
                <h1>Hello, welcome {user}</h1>
                <section className={styles.ProfileHeaderActions}>
                    <Link to="/translate">
                        <span className="material-icons">arrow_back</span>
                        Back to Translate
                    </Link>
                    <button onClick={logout}>
                        <span className="material-icons">logout</span>
                        Logout</button>
                    <button onClick={clear} className="bg-danger">
                        <span className="material-icons">delete</span>
                        Clear Translations
                    </button>
                </section>
            </AppContainer>
        </header>
    )
}
export default ProfileHeader