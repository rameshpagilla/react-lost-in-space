import { useEffect, useState } from "react"
import { useHistory } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import AppContainer from "../../hoc/AppContainer"
import { Storage } from "../../utils/storage"
import ColouredFooter from "../ColouredFooter/ColouredFooter"
import RoundButton from "../RoundButton/RoundButton"
import styles from './Login.module.css'
import LoginHeader from "./LoginHeader"

const Login = () => {

    const [username, setUsername] = useState()
    const history = useHistory()
    const [user, setUser] = useUser()

    const onInputChange = e => {
        setUsername(e.target.value.trim())
    }

    const onLoginSubmit = event => {
        event.preventDefault()
        Storage.save('username', username.trim())
        setUser(username)
    }

    useEffect(() => {
        if (user) {
            history.push('/translate')
        }
    }, [user, history])

    return (
        <main>

            <LoginHeader />

            <AppContainer>
                <section className={styles.LoginForm}>
                    
                    <form onSubmit={onLoginSubmit}>
                        <fieldset className={styles.LoginFormControls}>
                            <input onChange={onInputChange}
                                type="text"
                                placeholder="Whats your name?"
                                className={styles.LoginInput} />
                            <RoundButton type="submit" icon="arrow_forward" />
                        </fieldset>
                    </form>

                    <ColouredFooter />
                </section>
            </AppContainer>
        </main>
    )
}
export default Login