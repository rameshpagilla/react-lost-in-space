import AppContainer from '../../hoc/AppContainer'
import styles from './LoginHeader.module.css'
import HeaderLogin from './Logo.png'

const LoginHeader = () => {
    return (
        <header className={styles.LoginHeader}>
            <AppContainer>
                <section className={styles.LoginHeaderContent}>
                    <aside>
                        <img src={HeaderLogin} alt="" className={ styles.LoginHeaderLogo } />
                    </aside>
                    <div>
                        <h1 className={ styles.LoginHeaderTitle }>Lost in Translation</h1>
                        <h4 className={ styles.LoginHeaderSubTitle }>Get Started</h4>
                    </div>
                </section>

            </AppContainer>
        </header>
    )
}
export default LoginHeader