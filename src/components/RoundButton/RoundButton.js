import styles from './RoundButton.module.css'

const RoundButton = ({ icon, type = "submit" }) => {
    return (
        <button className={ styles.RoundButton } type={ type }>
            <span className="material-icons">{ icon }</span>
        </button>
    )
}
export default RoundButton