import styles from './ColouredFooter.module.css'

const ColouredFooter = ({ children }) => {
    return (
        <footer className={styles.ColouredFooter}>
            { children }
        </footer>
    )
}
export default ColouredFooter