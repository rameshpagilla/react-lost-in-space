import { useState } from "react"
import { Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { Storage } from "../../utils/storage"
import RoundButton from "../RoundButton/RoundButton"
import styles from './Translation.module.css'
import TranslationResult from "./TranslationResult"

const Translation = () => {

    const [username] = useState(Storage.get('username'))
    const [translation, setTranslation] = useState('')
    const [translationLetters, setTranslationLetters] = useState([])

    const onInputChange = e => {
        setTranslation(e.target.value)
    }

    const onTranslateSubmit = event => {
        event.preventDefault()
        const currentTranslations = Storage.get('translations') || []
        const lastTranslation = currentTranslations.slice(-1).pop()

        const { confirm } = window

        if ( // Check for repeated translation
            lastTranslation === translation &&
            !confirm(`Do you want to translate ${translation} again?`)) {
            return
        }

        currentTranslations.push(translation)
        Storage.save('translations', currentTranslations.slice(-10))
        setTranslationLetters(translation.split(''))
    }

    return (
        <main>
            {!username && <Redirect to="/" />}
            <form onSubmit={onTranslateSubmit} className={styles.TranslationForm}>
                <fieldset className={styles.TranslationFormControls}>
                    <input id="translation"
                        className={styles.TranslationInput}
                        onChange={onInputChange}
                        type="text"
                        placeholder="What do you want to translate?"
                        autoFocus={true} />
                    <RoundButton icon="arrow_forward" />
                </fieldset>
            </form>
            <AppContainer>
                <TranslationResult translationLetters={translationLetters} />
            </AppContainer>
        </main>
    )
}
export default Translation