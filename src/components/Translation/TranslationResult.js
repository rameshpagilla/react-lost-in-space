import ColouredFooter from '../ColouredFooter/ColouredFooter'
import styles from './TranslationResult.module.css'

const TranslationResult = ({ translationLetters = [] }) => {

    const renderImages = translationLetters.map((letter, idx) => {

        const lowerCaseLetter = letter.toLowerCase()
        const key = `${idx}-${lowerCaseLetter}`

        if (lowerCaseLetter.trim() === '') {
            return <span key={key} className={styles.TranslationSpace}>&nbsp;</span>
        }

        const regexNoSpecialChars = /[^A-Za-z]/

        if (lowerCaseLetter.match(regexNoSpecialChars) !== null) {
            return <span key={key} className={styles.TranslationSpecialChar}>{lowerCaseLetter}</span>
        }

        return <img key={key} src={process.env.PUBLIC_URL + `/assets/signs/${lowerCaseLetter}.png`} alt={key} />
    })

    return (
        <section className={styles.TranslationImages}>
            <p>{ renderImages }</p>
            { renderImages.length === 0 && <p>Try to translate something 😀</p> }
            <ColouredFooter><span>Translation</span></ColouredFooter>
        </section>
    )
}
export default TranslationResult