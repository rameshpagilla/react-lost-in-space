import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Login from './components/Login/Login';
import Translation from './components/Translation/Translation';
import Profile from './components/Profile/Profile';
import Header from './components/Header/Header';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />

        <Switch>
          <Route exact={true} path="/" component={Login} />
          <Route path="/translate" component={Translation} />
          <Route path="/profile" component={Profile} />
        </Switch>

      </div>
    </BrowserRouter>
  );
}

export default App;
