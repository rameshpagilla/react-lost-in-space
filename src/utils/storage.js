export class Storage {
    static save(key, value) {
        const json = JSON.stringify(value)
        const encoded = btoa(encodeURIComponent(json))
        localStorage.setItem(key, encoded)
    }

    static get(key) {
        const value = localStorage.getItem(key)
        if (!value) {
            return null
        }
        const decoded = decodeURIComponent(atob(value))
        return JSON.parse(decoded)
    }

    static delete(key) {
        localStorage.removeItem(key)
    }
}